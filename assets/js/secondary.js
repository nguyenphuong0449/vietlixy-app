var userLogged = localStorage.getItem("userLogged");
var user = userLogged === "" ? null : JSON.parse(userLogged);
var $soTienNhanDuoc = 0;

function getSoTienNhanDuoc($models) {
    $chiPhiChienDich = $models.chi_phi_chien_dich;
    $hoaHong = $models.chi_phi_chien_dich;
    $typeHoaHong = $models.type_hoa_hong;
    $soTienNhanDuoc = 0;

    if ($typeHoaHong === "Phần trăm")
        $soTienNhanDuoc = ($hoaHong * $chiPhiChienDich) / 100;
    else $soTienNhanDuoc = $hoaHong;
}

function ValidateEmail(email) {
    return email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
}

function validateFormUpdatProfile() {
    var $loi = 0;
    if ($("#full-name").val().trim() === '') {
        $loi++;
        $("#full-name").parent().find('p').removeClass('hidden').text('Vui lòng nhập họ tên');
    } else {
        $("#full-name").parent().find('p').addClass('hidden')
    }
    if ($("#email-profile").val().trim() === '') {
        $loi++;
        $("#email-profile").parent().find('p').removeClass('hidden').text('Vui lòng nhập Email');
    } else {
        if (!ValidateEmail($("#email-profile").val().trim())) {
            $loi++;
            $("#email-profile").parent().find('p').removeClass('hidden').text('Email không hợp lệ');
        } else
            $("#email-profile").parent().find('p').addClass('hidden');
    }
    if ($("#phone-number").val().trim() === '') {
        $loi++;
        $("#phone-number").parent().find('p').removeClass('hidden').text('Vui lòng nhập số điện thoại');
    } else {
        if ($("#phone-number").val().length != 10) {
            $loi++;
            $("#phone-number").parent().find('p').removeClass('hidden').text('Số điện thoại là 10 ký tự');
        } else {
            var $loiDT = 0;
            for (var $i = 0; $i < $("#phone-number").val().length; $i++) {
                if (isNaN(parseInt($("#phone-number").val()[$i])))
                    $loiDT++;
            }

            if ($loiDT > 0) {
                $loi++;
                $("#phone-number").parent().find('p').removeClass('hidden').text('Số điện thoại chỉ gồm số 0-9');
            } else
                $("#phone-number").parent().find('p').addClass('hidden');
        }
    }

    // Check đổi mật khẩu
    if ($("#password4").val().trim() !== '') {
        if (($("#new_password").val().trim() === '')) {
            $loi++;
            $("#new_password").parent().find('p').removeClass('hidden').text('Vui lòng nhập mật khẩu mới');
        } else {
            $("#new_password").parent().find('p').addClass('hidden');
        }
    } else
        $("#new_password").parent().find('p').addClass('hidden');

    if ($("#re_new_password").val().trim() !== $("#new_password").val().trim()) {
        $loi++;
        $("#re_new_password").parent().find('p').removeClass('hidden').text('Mật khẩu mới nhập lại không khớp');
    } else {
        $("#re_new_password").parent().find('p').addClass('hidden');
        if ($("#re_new_password").val().trim() !== '' && $("#password4").val().trim() === '') {
            $loi++;
            $("#password4").parent().find('p').removeClass('hidden').text('Vui lòng nhập mật khẩu hiện tại');
        } else
            $("#password4").parent().find('p').addClass('hidden');
    }

    return $loi;
}

var noiDungThongBao = document.getElementById("noi-dung-thong-bao");

var settings = {
    "url": "https://api.vietqr.io/v2/banks",
    "method": "GET",
    "timeout": 0,
    "headers": {
        "Cookie": "connect.sid=s%3Aw65X-PXYIAEiALOLVvp-abAK1OY065cc.aJuCyMsOVU9xheFyiprziDuDv1qWAM9epWUz7V1VEFM"
    },
};

$(document).ready(function () {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/get-contact",
        data: {},
        dataType: "json",
        type: "post",
        beforeSend: function () {
            $("#row-content-app-blog").html("");
        },
        success: function (data) {
            $("#lien_he_admin").html(`
        <div class="float-icon-hotline">
            <ul class ="left-icon hotline">
                <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton" href="`+ data.zalo +`"><i class="fa fa-zalo animated infinite tada"></i><span>Zalo</span></a></li>
                <li class="hotline_float_icon"><a target="_blank" rel="nofollow" id="messengerButton" href="`+ data.facebook +`"><i class="fa fa-messenger animated infinite tada"></i><span>Facebook</span></a></li>
            </ul>
    </div>
        `)
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });


});

function getData() {
    var search = $('#search-blog').val()
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-blog",
        data: {
            search
        },
        dataType: "json",
        type: "post",
        beforeSend: function () {
            $("#row-content-app-blog").html("");
        },
        success: function (data) {
            $.each(data.chienDichs, function (k, obj) {
                getSoTienNhanDuoc(obj);
                $("#row-content-app-blog").append(
                    `<div class="col-6 mb-2">
            <a href="#" class="btn-xem-chi-tiet" data-value=${obj.id} data-type="Kiếm tiền">
              <div class="blog-card blog-kiem-tien">
                <img
                  src=${obj.image}
                  alt="image"
                  class="imaged-blog w-100"
                />
                <div class="text">
                  <h4 class="title">
                    ${obj.title}
                  </h4>
                  <h3 id="pricing-offer-chi-tiet" class="text-center">+ <span  class="text-danger font-h3-big">` +
                    Number($soTienNhanDuoc).toLocaleString("vi") +
                    `</span> VNĐ</h3>
                </div>
              </div>
            </a>
          </div>`
                );
            });
            if (data.count < 6) {
                $("#load-more-app-blog-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
}

// App-blog
if ($("#body-app-blog").length > 0) {
    getData();
    $(document).on('change', '#search-blog', function (e) {
        getData()
    })
}

$("#load-more-app-blog").click(function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-blog",
        data: {page: ~~($(".blog-card").length / 6)},
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            $.each(data.chienDichs, function (k, obj) {
                getSoTienNhanDuoc(obj);
                $("#row-content-app-blog").append(
                    `<div class="col-6 mb-2">
            <a href="#" class="btn-xem-chi-tiet" data-value=${obj.id} data-type="Kiếm tiền">
              <div class="blog-card blog-kiem-tien">
                <img
                  src=${obj.image}
                  alt="image"
                  class="imaged-blog w-100"
                />
                <div class="text">
                  <h4 class="title">
                    ${obj.title}
                  </h4>
                  <h3 id="pricing-offer-chi-tiet" class="text-center">+ <span  class="text-danger font-h3-big">` +
                    Number($soTienNhanDuoc).toLocaleString("vi") +
                    `</span> VNĐ</h3>
                </div>
              </div>
            </a>
          </div>`
                );
            });
            if (data.count < 6) {
                $("#load-more-app-blog-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
});

// App-service
if ($("#body-app-service").length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-category",
        data: {type: "Dịch vụ"},
        dataType: "json",
        type: "post",
        beforeSend: function () {
            $("#row-content-app-service").html("");
        },
        success: function (data) {
            $.each(data.services, function (k, obj) {
                $("#row-content-app-service").append(
                    `<a href="#" class="item btn-list-campain" data-value=${obj.id} data-type="Dịch vụ">
            <div class="detail">
              <img src=${obj.image} alt="img" class="image-block imaged w48">
              <div>
                  <strong>${obj.name}</strong>
              </div>
            </div>
          </a>
          `
                );
            });
            if (data.count < 6) {
                $("#load-more-app-service-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
}

$("#load-more-app-service").click(function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-service",
        data: {page: ~~($(".blog-card").length / 6), type: "Dịch vụ"},
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            $.each(data.services, function (k, obj) {
                $("#row-content-app-service").append(
                    `<a href="#" class="item btn-item-list-campain" data-value=${obj.id} data-type="Dịch vụ">
            <div class="detail">
              <img src=${obj.image} alt="img" class="image-block imaged w48">
              <div>
                  <strong>${obj.name}</strong>
              </div>
            </div>
          </a>
          `
                );
            });

            if (data.count < 6) {
                $("#load-more-app-service-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
});

$(document).on("click", ".btn-xem-chi-tiet", function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/detail",
        data: {
            // uid: $user.uid,
            // auth: $user.auth,
            value: $(this).attr("data-value"),
            type: $(this).attr("data-type"),
        },
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            localStorage.setItem("models", JSON.stringify(data.models));
            localStorage.setItem("titlePage", JSON.stringify(data.title));
            window.location = "https://vietlixi.com/" + data.router;
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
});

$(document).on("click", ".btn-list-campain", function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/detail",
        data: {
            value: $(this).attr("data-value"),
            type: $(this).attr("data-type"),
        },
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            localStorage.setItem("models", JSON.stringify(data.models));
            localStorage.setItem("titlePage", JSON.stringify(data.title));
            window.location = "https://vietlixi.com/" + data.router;
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
});

// App-transactions
if ($("#body-app-transactions").length > 0) {
    var titlePage = localStorage.getItem("titlePage");
    var models = JSON.parse(localStorage.getItem("models"));

    $("title, .pageTitle").text(titlePage.replaceAll('"', ""));
    if (models.length > 0) {
        $.each(models, function (k, obj) {
            $("#row-content-app-transactions").append(
                `<a href="#" class="item btn-xem-chi-tiet-chien-dich" data-value=${obj.id} data-type="Vay tiền mùa dịch">
          <div class="detail">
            <img src=${obj.image} alt="img" class="image-block imaged w48">
            <div class="row">
                <div class="row">
                <div class="col-12">
                    <strong>${obj.title}</strong>
                </div>
                </div>
                <div class="row">
                <div class="col-12 tien-thuong` + k + `">
                </div>
                </div>
            </div>
            
          </div>
        </a>
        `
            );

            if (obj.ti_le_hoan_tien === null) {
                $('.tien-thuong' + k).append('')
            } else {
                if (obj.type_hoan_tien === "Tiền mặt") {
                    $('.tien-thuong' + k).append(`<span  class="text-success">Thưởng ngay ${Number(obj.ti_le_hoan_tien).toLocaleString("vi")} VNĐ khi đăng kí.</span>`)
                }
                if (obj.type_hoan_tien === "Phần trăm") {
                    $('.tien-thuong' + k).append(`<span  class="text-success">Hoàn tiền lên đến ${Number(obj.ti_le_hoan_tien).toLocaleString("vi", {
                        minimumFractionDigit: 2,
                        maximumFractionDigits: 2
                    })}% khi đăng kí.</span>`)
                }
            }

        });
    } else {
        $("#row-content-app-transactions").append(
            `<a>
        <div class="detail">
          <div>
              <strong>Không có chiến dịch tồn tại</strong>
          </div>
        </div>
      </a>
      `
        );
    }
}

// App-refunds
if ($("#body-app-refunds").length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-category",
        data: {type: "Thương hiệu"},
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            $.each(data.services, function (k, obj) {
                $("#row-content-app-refunds").append(
                    `<a href="#" class="item btn-xem-chi-tiet" data-value=${obj.id} data-type="Hoàn tiền Shopback">
            <div class="detail">
              <img src=${obj.image} alt="img" class="image-block imaged w48">
              <div>
                  <strong>${obj.name}</strong>
              </div>
            </div>
            <div class="right">
            <div class="price text-danger">Hoàn tiền ` +
                    obj.ti_le_hoan_tien +
                    `%</div>
        </div>
          </a>
          `
                );
            });

            if (data.count < 6) {
                $("#load-more-app-refunds-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
}

$("#load-more-app-refunds").click(function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-category",
        data: {page: ~~($(".blog-card").length / 6), type: "Thương hiệu"},
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            $.each(data.services, function (k, obj) {
                $("#row-content-app-refunds").append(
                    `<a href="#" class="item btn-xem-chi-tiet" data-value=${obj.id} data-type="Hoàn tiền Shopback">
            <div class="detail">
              <img src=${obj.image} alt="img" class="image-block imaged w48">
              <div>
                  <strong>${obj.name}</strong>
              </div>
            </div>
            <div class="right">
              <div class="price text-danger">Hoàn tiền ` +
                    obj.ti_le_hoan_tien +
                    `%</div></div>
          </a>
          `
                );
            });

            if (data.count < 6) {
                $("#load-more-app-service-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
});

$(document).on("click", ".btn-xem-chi-tiet-chien-dich", function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/detail",
        data: {
            value: $(this).attr("data-value"),
            type: $(this).attr("data-type"),
        },
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            localStorage.setItem("models", JSON.stringify(data.models));
            localStorage.setItem("titlePage", JSON.stringify(data.title));
            window.location = "https://vietlixi.com/" + data.router;
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
});

// App-refunds
if ($("#body-app-transaction-history").length > 0) {
    if (user) {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/app-transaction-history",
            data: {user},
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                if (data.transactions.length > 0) {
                    $.each(data.transactions, function (k, obj) {
                        const classText = obj.trang_thai_postback == 'Khởi tạo' ? 'text-warning' : (obj.trang_thai_postback == 'Hoạt động' ? 'text-success' : 'text-danger');

                        $("#row-content-app-transaction-history").append(
                            `<a href="#" class="item btn-chi-tiet-transaction" data-value=${obj.id}>
                <div class="detail">
                  <img src=${obj.image} alt="img" class="image-block imaged w48">
                  <div>
                      <strong>${obj.title}</strong>
                      <p><strong class=${classText}>${obj.trang_thai_postback}</strong></p>
                      <p>${obj.nguoi_thuc_hien}</p>
                  </div>
                </div>
              </a>
              `
                        );
                    });
                } else {
                    $("#row-content-app-transaction-history").append(
                        `<div class="detail">
              <div>Không có lịch sử</div>
            </div>
            `
                    );
                }

                if (data.count < 6) {
                    $("#load-more-app-transaction-history-div").addClass("hidden");
                }
            },
            error: function (r1, r2) {
                console.log(r1.responseJSON.message);
            },
        });
    } else {
        $("#row-content-app-transaction-history").append(
            `<div class="detail">
        <div>Vui lòng đăng nhập</div>
      </div>
      `
        );
        $("#load-more-app-transaction-history-div").addClass("hidden");
    }
}

$("#load-more-app-transaction-history").click(function (e) {
    e.preventDefault();
    if (user) {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/app-transaction-history",
            data: {page: ~~($(".btn-chi-tiet-transaction").length / 6), user},
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                $.each(data.transactions, function (k, obj) {
                    const classText = obj.trang_thai_postback == 'Khởi tạo' ? 'text-warning' : (obj.trang_thai_postback == 'Hoạt động' ? 'text-success' : 'text-danger');
                    $("#row-content-app-transaction-history").append(
                        `<a href="#" class="item btn-chi-tiet-transaction" data-value=${obj.id}>
              <div class="detail">
                <img src=${obj.image} alt="img" class="image-block imaged w48">
                <div>
                    <strong>${obj.title}</strong>
                    <p><strong class=${classText}>${obj.trang_thai_postback}</strong></p>
                    <p>${obj.nguoi_thuc_hien}</p>
                </div>
              </div>
            </a>
            `
                    );
                });

                if (data.count < 6) {
                    $("#load-more-app-transaction-history-div").addClass("hidden");
                }
            },
            error: function (r1, r2) {
                console.log(r1.responseJSON.message);
            },
        });
    } else {
        $("#row-content-app-transaction-history").append(
            `<div class="detail">
        <div>Vui lòng đăng nhập</div>
      </div>
      `
        );
        $("#load-more-app-transaction-history-div").addClass("hidden");
    }
});

$(document).on("click", ".btn-chi-tiet-transaction", function (e) {
    e.preventDefault();
    if (user) {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/transaction-detail",
            data: {
                id: $(this).attr("data-value"),
                user,
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                localStorage.setItem("model", JSON.stringify(data.model));
                localStorage.setItem("titlePage", JSON.stringify(data.model.title));
                window.location = "https://vietlixi.com/" + data.router;
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    }
});

if ($("#body-transaction-detail-app").length > 0) {
    if (user) {
        var titlePage = localStorage.getItem("titlePage");
        var model = JSON.parse(localStorage.getItem("model"));

        $("title, .pageTitle").text(titlePage.replaceAll('"', ""));

        $("#transaction-detail-info").html(`
      <div class="icon-wrapper">
          <div class="iconbox">
              <img src=${model.image} alt='img' style="width: 80px;border-radius: 40px">
          </div>
      </div>
      <h3 class="text-center mt-2">${model.title}</h3>
    `);

        var date = new Date(model.ngay_thuc_hien);
        var total = model.so_tien_nhan_duoc ? model.so_tien_nhan_duoc : 0;
        $("#transaction-detail-content").html(`
      <li>
        <strong>Trạng thái</strong>
        <span class=${
            model.trang_thai_postback === "Khởi tạo"
                ? "text-warning"
                : model.trang_thai_postback === "Không hoạt động"
                    ? "text-danger"
                    : "text-success"
        }>${model.trang_thai_postback}</span>
      </li>
      <li>
          <strong>Ngày thực hiện</strong>
          <span>${date.getDate()}/${
            date.getMonth() + 1
        }/${date.getFullYear()}</span>
      </li>
    `);
        if (model.trang_thai_postback === "Hoạt động") {
            $("#transaction-detail-content").append(`
        <li>
          <strong>Tiền nhận được</strong>
          <span>${total.toLocaleString("vi")} VNĐ</span>
        </li>
      `);
        }
    }
}

if ($("#body-app-transaction-history-general").length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/app-history",
        data: {page: ~~($(".blog-card").length / 6)},
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            $.each(data.model, function (k, obj) {
                $("#row-content-app-transaction-history-general").append(
                    `<a href="#" class="item btn-xem-chi-tiet" data-value="` +
                    obj.id +
                    `" data-type="Hoàn tiền Shopback" class="btn-xem-chi-tiet">
                              <div class="detail">
                                  <div>
                                      <strong>` +
                    obj.username +
                    `</strong><p>${obj.type}</p>
                                  </div>
                              </div>
                              <div class="right">
                                  <div class="price text-danger">` +
                    (+obj.cost).toLocaleString('vi', {minimumFractionDigits: 0, maximumFractionDigits: 0}) +
                    ` VNĐ</div>
                              </div>
                          </a>`
                );
            });
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
}

// App bill
if ($("#body-app-bills").length > 0) {
    if (user) {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/lich-su-rut-tien",
            data: {
                user,
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                $.each(data.choDuyets, function (k, obj) {
                    var date = new Date(obj.created);
                    $("#row-waiting").append(
                        `<div class="col-6 mb-2">
              <div class="bill-box">
                <div class="price">${Number(obj.cost).toLocaleString("vi")} VNĐ</div>
                <div style="text-align: left">NTH: ${date.getDate()}/${
                            date.getMonth() + 1
                        }/${date.getFullYear()}</div>
                <div style="text-align: left">Ghi chú: ${obj.ghi_chu ? obj.ghi_chu : ''}</div>
                <div style="text-align: left">Thực hiện: ${obj.username}</div>
              </div>
            </div>
            `
                    );
                });
                $.each(data.lichSus, function (k, obj) {
                    var date = new Date(obj.created);
                    const classText = obj.trang_thai == 'Chờ duyệt' ? 'text-warning' : (obj.trang_thai == 'Đã duyệt' ? 'text-success' : 'text-danger');
                    $("#row-history").append(
                        `<div class="col-6 mb-2">
              <div class="bill-box">
                <div class="price">${Number(obj.cost).toLocaleString("vi")} VNĐ</div>
                <div style="text-align: left">NTH: ${date.getDate()}/${
                            date.getMonth() + 1
                        }/${date.getFullYear()}</div>
                <div style="text-align: left">Ghi chú: ${obj.ghi_chu ? obj.ghi_chu : ''}</div>
                <div style="text-align: left">Thực hiện: ${obj.username}</div>
                <div style="text-align: left">Trạng thái: <div class=${classText}>${obj.trang_thai}</div></div>
              </div>
            </div>
            `
                    );
                });
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    }
}
// Thống kê rút tiền
if ($("#body-thong-ke-rut-tien").length > 0) {
    if (user) {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/get-thong-ke-rut-tien",
            data: {
                user,
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                for (const trangThai in data) {
                    console.log(trangThai)

                    $('#list-rut-tien-history').append('<div class="section mt-2"><div class="section-title">' + trangThai + '</div><div class="transactions">');
                    console.log(data[trangThai][length])
                    if (!data[trangThai][length]) {
                        $("#list-rut-tien-history").append(
                            `<p style="padding-left: 16px">Bạn không có giao dịch nào</p>`
                        )
                    } else {
                        $.each(data[trangThai], function (k, obj) {
                            var date = new Date(obj.created);

                            $("#list-rut-tien-history").append(
                                `<a href="#" class="item item-rut-tien block-display" data-trangthai="` + trangThai + `" data-value="` + obj.id + `" data-type="postback">
                                <div class="row flex-wrap">
                                    <div class="col-12">
                                      <div class="row">
                                          <div class="col-12"><span class="text-primary">Ngày thực hiện: </span<strong>${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}</strong></div>
                                          
                                      </div>
                              <div class="row flex-wrap">
                                  <div class="col-7"">
                                      <span>Ngân hàng: ` + obj.bank_name + ` </span>
                                  </div>
                                  <div class="col-5">
                                    <div class="price text-danger text-right"> <span class="` + (Number(obj.cost) === 0 ? 'text-danger' : 'text-success') + `">`
                                + Number(obj.cost).toLocaleString('vi') + `đ</span>
                                    </div>
                                  </div>
                              </div>
                            </div>
                            </div>
                                </a>`);
                        })
                        $('#list-rut-tien-history').append('</div></div>');
                    }
                }
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    }

    $(document).on('click', '.item-rut-tien', function (e) {
        e.preventDefault();
        console.log('hellooooo')
        if ($(this).attr('data-trangthai') === 'Chờ duyệt') {
            $(".item-cho-duyet").removeClass('hidden');
        } else {
            $(".item-cho-duyet").addClass('hidden');
        }
        $("#actionSheet-hidden-value").val($(this).attr('data-value'));
        $("#actionSheet").modal('show');
    })

    $(document).on('click', '#action-xem-chi-tiet-rut-tien', function (e) {
        e.preventDefault();
        $.ajax({
            url: "https://app.vietlixi.com/api/service/load-rut-tien-for-update",
            dataType: "json",
            type: "post",
            data: {
                user,
                rutTien: $("#actionSheet-hidden-value").val()
            },
            beforeSend: function () {

            },
            success: function (data) {
                var date = new Date(data.rutTien.created);
                var $thongTinRutTien =
                    `
               <div>
                    <div class="row flex-wrap">
                        <div class="col-4"><h4 style="display: inline-block !important;">Ngày tạo: </h4></div>
                        <div class="col-8"><span>${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}</span></div>
                    </div>
                    <div class="row flex-wrap">
                        <div class="col-4"><h4 style="display: inline-block !important;">Số tiền: </h4></div>
                        <div class="col-8"><span>` + (Number(data.rutTien.cost).toLocaleString('vi')) + ' VNĐ' + `</span></div>
                    </div>
                     <div class="row flex-wrap">
                        <div class="col-4"><h4 style="display: inline-block !important;">Tên ngân hàng: </h4></div>
                        <div class="col-8"><span>` + data.rutTien.bank_name + `</span></div>
                    </div> 
                    <div class="row flex-wrap">
                        <div class="col-4"><h4 style="display: inline-block !important;">Số tài khoản: </h4></div>
                        <div class="col-8"><span>` + data.rutTien.bank_number + `</span></div>
                    </div>
              </div>`;
                $("#thong-tin-chi-tiet-rut-tien").html($thongTinRutTien);

                $("#actionSheet").modal('hide');
                $("#actionXemChiTietRutTien").modal('show');
            },
            error: function (r1, r2) {
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show')
            },
        });
    });

    $(document).on('click', '#action-sua-rut-tien', function (e) {
        e.preventDefault();
        $.ajax(settings).done(function (response) {
            console.log(response);

            $.ajax({
                url: "https://app.vietlixi.com/api/service/load-rut-tien-for-update",
                dataType: "json",
                type: "post",
                data: {
                    user,
                    rutTien: $("#actionSheet-hidden-value").val()
                },
                beforeSend: function () {
                },
                success: function (data) {
                    var $thongTinRutTienSua =
                        `
                    <div>
                        <div class="row flex-wrap">
                            <span>Số tiền: </span><input class="form-control" id="so-tien-sua" type="text" style="margin: 0px 0px 10px 10px; padding-left: 10px"    >
                        </div>
                        <div class="row flex-wrap">
                            <label for="bank-name-dropdown" >Tên ngân hàng</label>
                            <select name="bank-name-dropdown" id="bank-name-dropdown" class="form-select" style="margin-left: 10px; border-bottom: 1px solid #dcdce9;">
                            </select>
                        </div>
                        <div class="row flex-wrap">
                            <span>Số tài khoản: </span><input class="form-control" id="so-tai-khoan" type="number" style="margin: 0px 0px 10px 10px;padding-left: 10px">
                        </div>
                </div>
               `;
                    $("#thong-tin-rut-tien").html($thongTinRutTienSua);
                    $("#so-tien-sua").val(Number(data.rutTien.cost).toLocaleString('vi'))
                    $("#so-tai-khoan").val(data.rutTien.bank_number)
                    $.each(response.data, function (k, value) {
                        $('#bank-name-dropdown').append(`<option value="` + value.shortName + `">` + value.shortName + `</option>`)
                    })
                    $("#bank-name-dropdown").val(data.rutTien.bank_name)


                    $("#actionSheet").modal('hide');
                    $("#actionUpdateRutTien").modal('show');
                },
                error: function (r1, r2) {
                    $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                    $("#DialogIconedDanger").modal('show')
                },
            });
        });
    });

    $(document).on('click', '#btn-close-update-rut-tien', function (e) {
        $("#actionUpdateRutTien").modal('hide');
    })

    $(document).on('click', '#luu-thong-tin-update-rut-tien', function (e) {
        e.preventDefault()
        $.ajax({
            url: "https://app.vietlixi.com/api/service/update-rut-tien",
            dataType: "json",
            type: "post",
            data: {
                user,
                rutTien: $("#actionSheet-hidden-value").val(),
                soTien: $("#so-tien-sua").val(),
                tenNganHang: $("#bank-name-dropdown").val(),
                soTaiKhoan: $("#so-tai-khoan").val()
            },
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === 'success') {
                    $("#DialogIconedDanger .modal-icon").removeClass('text-danger').addClass('text-success');
                    $("#DialogIconedDanger .modal-icon").html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                    $('#actionUpdateRutTien').modal('hide');

                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                } else {
                    $("#DialogIconedDanger .modal-icon").removeClass('text-success').addClass('text-danger');
                    $("#DialogIconedDanger .modal-icon").html('<ion-icon name="close-circle"></ion-icon>');

                    $('#actionUpdateRutTien').modal('hide');
                }

                $('#DialogIconedDanger #noi-dung-thong-bao').html(data.content);
                $('#DialogIconedDanger').modal('show');
            },
            error: function (r1, r2) {
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show')
            },
        });

    })

    $(document).on('click', '#action-huy-rut-tien', function (e) {
        e.preventDefault();

        $.ajax({
            url: "https://app.vietlixi.com/api/service/huy-rut-tien",
            dataType: "json",
            type: "post",
            data: {
                user,
                rutTien: $("#actionSheet-hidden-value").val()
            },
            beforeSend: function () {
            },
            success: function (data) {
                $("#modal-icon")
                    .removeClass('text-danger')
                    .addClass('text-success')
                    .html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                $("#noi-dung-thong-bao").html(data.message);
                $("#DialogIconedDanger").modal('show');

                setTimeout(function () {
                    location.reload();
                }, 2000);
            },
            error: function (r1, r2) {
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show')
            },
        });
    })


    $("#so-tien-sua").on('keyup', function () {
        var n = parseInt($(this).val().replace(/\D/g, ''));
        $(this).val(n.toLocaleString());
    });

}

if ($("#body-bao-cao-don-hang").length > 0) {
    if (user) {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/get-bao-cao-don-hang",
            data: {
                user,
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
                $("#list-transactions").html('');
            },
            success: function (data) {
                $.each(data.data, function (k, obj) {
                    $("#list-transactions").append(`
          <a href="#" class="item xem-chi-tiet-postback block-display" data-value="` + obj.id + `" data-type="postback">
            <div class="row">
                <div class="col-3">
                  <div class="text-center"><img src=` + obj.image + ` alt=` + obj.title + ` class="image-block imaged w48"></div>
                </div>
                <div class="col-9">
                    <strong>` + obj.title + `</strong>
                  <div class="row">
                      <div class="col-4"><p>` + obj.trang_thai + `</p></div>
                      <div class="col-8">
                        <div class="price text-danger text-right"> +<span class="` + (Number(obj.so_tien_nhan_duoc) == 0 ? 'text-danger' : 'text-success') + `">`
                        + Number(obj.so_tien_nhan_duoc).toLocaleString('vi') + `đ</span>/<span class="color-black">`
                        + Number(obj.chi_phi_chien_dich).toLocaleString('vi') + `đ</span>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
          </a>
`)
                });
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    }
}

$(document).on('click', '.xem-chi-tiet-postback', function (e) {
    e.preventDefault();
    $.ajax({
        url: "https://app.vietlixi.com/api/service/postback-detail",
        data: {
            user,
            postback: $(this).attr('data-value')
        },
        dataType: "json",
        type: "post",
        beforeSend: function () {
        },
        success: function (data) {
            var i = 0;
            for (var key of Object.keys(data.results)) {
                $str = '<div class="section-title">' +
                    '<div class="row">' +
                    '<div class="col-4">' + data.results[key].ngay + '</div>' +
                    '<div class="col-8 text-right">' + Number(data.tongNgay[i].tong_tien).toLocaleString('vi') + 'đ</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="transactions">';
                i++;
                $.each(data.results[key].data, function (k, obj) {
                    $str += `<a href="#" class="item block-display">
                    <div class="row">
                        <div class="col-3"><img src="` + obj.image + `" alt="` + obj.title + `" class="image-block imaged w48"></div>
                        <div class="col-9">
                          <div>
                              <strong>` + obj.title + `</strong>
                              <div class="row">
                                <div class="col-4">
                                    <p>Duyệt: ` + obj.nguoi_duyet + `</p>
                                </div>
                                <div class="col-8">
                                    <div class="price text-right text-` + data.color + `"><ion-icon name="` + data.icon + `"></ion-icon>` + Number(obj.cost).toLocaleString('vi') + `đ</div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </a>`
                });
                $("#appCapsule").append('<div class="section mt-2">' + $str + '</div></div>');
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
})

if ($("#my-card").length > 0) {
    console.log(user)
    if (user === null)
        window.location = 'https://vietlixi.com/app-login.html';
    else {
        $.ajax({
            url: "https://app.vietlixi.com/api/service/tong-hop-vi-cua-toi",
            data: {
                user,
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                $.each(data, function (k, obj) {
                    for (var key of Object.keys(obj)) {
                        if (key !== 'ngayGanNhat' && key !== 'soLuong')
                            $("#" + k + "-" + key).text(Number(obj[key]).toLocaleString('vi') + 'đ');
                        else
                            $("#" + k + "-" + key).text(obj[key]);
                    }
                })
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    }

}

// Xem chi tiết ví
$(document).on('click', '.xem-chi-tiet-vi', function (e) {
    e.preventDefault();
    localStorage.setItem("xemChiTietVi", $(this).attr('data-type'));
    window.location = "https://vietlixi.com/app-transactions-vi.html";
})

if ($("#app-transactions-vi").length > 0) {

    $(".pageTitle").html('Giao dịch ' + localStorage.getItem("xemChiTietVi"));

    $.ajax({
        url: "https://app.vietlixi.com/api/service/chi-tiet-giao-dich",
        data: {
            user,
            type: localStorage.getItem("xemChiTietVi")
        },
        dataType: "json",
        type: "post",
        beforeSend: function () {
            $("#appCapsule").html('');
        },
        success: function (data) {
            var i = 0;
            for (var key of Object.keys(data.results)) {
                $str = '<div class="section-title">' +
                    '<div class="row">' +
                    '<div class="col-4">' + data.results[key].ngay + '</div>' +
                    '<div class="col-8 text-right">' + Number(data.tongNgay[i].tong_tien).toLocaleString('vi') + 'đ</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="transactions">';
                i++;
                $.each(data.results[key].data, function (k, obj) {
                    $str += `<a href="#" class="item block-display">
                    <div class="row">
                        <div class="col-3"><img src="` + obj.image + `" alt="` + obj.title + `" class="image-block imaged w48"></div>
                        <div class="col-9">
                          <div>
                              <strong>` + obj.title + `</strong>
                              <div class="row">
                                <div class="col-4">
                                    <p>Duyệt: ` + obj.nguoi_duyet + `</p>
                                </div>
                                <div class="col-8">
                                    <div class="price text-right text-` + data.color + `"><ion-icon name="` + data.icon + `"></ion-icon>` + Number(obj.cost).toLocaleString('vi') + `đ</div>
                                </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </a>`
                });
                $("#appCapsule").append('<div class="section mt-2">' + $str + '</div></div>');
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
}

if ($("#body-doi-qua").length > 0) {

    $.ajax({
        url: "https://app.vietlixi.com/api/service/get-gifts",
        dataType: "json",
        type: "post",
        beforeSend: function () {
            $("#appCapsule #waiting .row").html('');
        },
        success: function (data) {
            $.each(data, function (k, obj) {
                $("#appCapsule #waiting .row").append(`<div class="col-6 mb-2">
              <div class="bill-box">
                  <div class="img-wrapper">
                      <img src="https://app.vietlixi.com/api/images/` + obj.anh_dai_dien + `" alt="` + obj.name + `" class="image-block imaged w48">
                  </div>
                  <div class="price">` + (Number(obj.gia_tri).toLocaleString('vi')) + `</div>
                  <p>` + obj.name + `</p>
                  <a href="#" data-value="` + obj.id + `" class="btn btn-primary btn-block btn-sm btn-doi-qua">Đổi quà ngay</a>
              </div>
          </div>`);
            })
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });

    $(document).on('click', '.btn-doi-qua', function (e) {
        e.preventDefault();

        $.ajax({
            url: "https://app.vietlixi.com/api/service/nhan-qua",
            data: {
                user,
                quaTang: $(this).attr('data-value')
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === 'success') {
                    var $thongTinQuaTang =
                        `<input type="hidden" value="` + data.quaTang.id + `" name="quaTangID" id="qua-tang-id">
              <div class="bill-box">
                  <div class="img-wrapper">
                      <img src="https://app.vietlixi.com/api/images/` + data.quaTang.anh_dai_dien + `" alt="` + data.quaTang.name + `" class="image-block imaged w140">
                  </div>
                  <div class="price">` + (Number(data.quaTang.gia_tri).toLocaleString('vi')) + `</div>
                  <h3>` + data.quaTang.name + `</h3>
              </div>`;
                    $("#block-thong-tin-qua-tang").html($thongTinQuaTang);
                    $("#DialogFormDoiQua").modal('show');
                } else {
                    localStorage.setItem("currentUrl", window.location.href);
                    $('#DialogIconedDanger #noi-dung-thong-bao').html(data.content);
                    $('#DialogIconedDanger').modal('show');
                    var $time = data.time;
                    setInterval(function () {
                        $("#thoi-gian").text($time);
                        $time--;
                    }, 1000);
                    setTimeout(function () {
                        window.location = "https://vietlixi.com/app-login.html";
                    }, 10000);
                }

            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    })

    $(document).on('click', '.btn-thuc-hien-nhan-qua', function (e) {
        e.preventDefault();
        if ($("#so-luong-qua").val() == '' || parseInt($("#so-luong-qua").val()) <= 0) {
            $("#error-quantity").removeClass('hidden').html('Số lượng tối thiểu 1.')
            $("#so-luong-qua").focus();
        } else {
            $("#error-quantity").addClass('hidden');
            $.ajax({
                url: "https://app.vietlixi.com/api/service/nhan-doi-qua",
                data: {
                    soLuong: $("#so-luong-qua").val(),
                    quaTang: $("#qua-tang-id").val(),
                    user
                },
                dataType: "json",
                type: "post",
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status === 'success') {
                        $("#DialogIconedDanger .modal-icon").removeClass('text-danger').addClass('text-success');
                        $("#DialogIconedDanger .modal-icon").html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                        $("#DialogFormDoiQua").modal('hide');
                    } else {
                        $("#DialogIconedDanger .modal-icon").removeClass('text-success').addClass('text-danger');
                        $("#DialogIconedDanger .modal-icon").html('<ion-icon name="close-circle"></ion-icon>');
                    }

                    $('#DialogIconedDanger #noi-dung-thong-bao').html(data.content);
                    $('#DialogIconedDanger').modal('show');
                },
                error: function (r1, r2) {
                    noiDungThongBao.innerText = r1.responseJSON.message;
                    DialogIconedDanger.show();
                },
            });
        }
    })
}

$(document).on('click', '.btn-rut-tien', function (e) {
    e.preventDefault();
    if ($("#so-tien").val() === '' || parseInt($("#so-tien").val()) <= 0) {
        $("#error-quantity").removeClass('hidden').html('Vui lòng nhập số tiền muốn rút.')
        $("#so-tien").focus();
    } else {
        $("#error-quantity").addClass('hidden');
        $.ajax({
            url: "https://app.vietlixi.com/api/service/nhan-rut-tien",
            data: {
                soTien: $("#so-tien").val(),
                tenNganHang: $("#bank-name").val(),
                soTaiKhoan: $("#bank-number").val(),
                user
            },
            dataType: "json",
            type: "post",
            beforeSend: function () {
            },
            success: function (data) {
                if (data.status === 'success') {
                    $("#DialogIconedDanger .modal-icon").removeClass('text-danger').addClass('text-success');
                    $("#DialogIconedDanger .modal-icon").html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                    $("#DialogFormDoiQua").modal('hide');
                } else {
                    $("#DialogIconedDanger .modal-icon").removeClass('text-success').addClass('text-danger');
                    $("#DialogIconedDanger .modal-icon").html('<ion-icon name="close-circle"></ion-icon>');
                }

                $('#DialogIconedDanger #noi-dung-thong-bao').html(data.content);
                $('#DialogIconedDanger').modal('show');
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        });
    }
})

if ($("#body-thong-ke-doi-qua").length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/get-thong-ke-doi-qua",
        dataType: "json",
        type: "post",
        data: {
            user,
        },
        beforeSend: function () {
            $("#list-transactions").html('');
        },
        success: function (data) {
            for (const trangThai in data) {
                $('#list-transactions').append('<div class="section mt-2"><div class="section-title">' + trangThai + '</div><div class="transactions">');
                var myStr = ([] === data[trangThai]) ? 11 : '';
                console.log(data[trangThai][length])
                if (!data[trangThai][length]) {
                    $("#list-transactions").append(
                        `<p style="padding-left: 16px">Bạn không có giao dịch nào</p>`
                    )
                } else {
                    $.each(data[trangThai], function (k, obj) {
                        $("#list-transactions").append(
                            `<a href="#" class="item item-nhan-qua block-display" data-trangthai="` + trangThai + `" data-value="` + obj.id + `" data-type="postback">
                <div class="row">
                    <div class="col-3">
                      <div class="text-center">
                          <img src=https://app.vietlixi.com/images/` + obj.anh_dai_dien + ` alt=` + obj.name + ` class="image-block imaged w48">
                      </div>
                    </div>
                    <div class="col-9">
                      <div><strong>` + obj.name + `</strong></div>
                      <div class="row">
                          <div class="col-7">
                              <p><strong>Đơn giá:</strong> ` + Number(obj.don_gia).toLocaleString('vi') + `; <strong>SL: </strong>` + obj.so_luong + `</p>
                          </div>
                          <div class="col-5">
                            <div class="price text-danger text-right"> <span class="` + (Number(obj.tong_tien) === 0 ? 'text-danger' : 'text-success') + `">`
                            + Number(obj.tong_tien).toLocaleString('vi') + `đ</span>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </a>`);
                    })
                    $('#list-transactions').append('</div></div>');
                }
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });

    $(document).on('click', '.item-nhan-qua', function (e) {
        e.preventDefault();
        if ($(this).attr('data-trangthai') === 'Chờ duyệt') {
            $(".item-cho-duyet").removeClass('hidden');
        } else {
            $(".item-cho-duyet").addClass('hidden');
        }
        $("#actionSheet-hidden-value").val($(this).attr('data-value'));
        $("#actionSheet").modal('show');
    })

    $(document).on('click', '#action-sua-nhan-qua', function (e) {
        e.preventDefault();
        $.ajax({
            url: "https://app.vietlixi.com/api/service/load-qua-tang-for-update",
            dataType: "json",
            type: "post",
            data: {
                user,
                quaTang: $("#actionSheet-hidden-value").val()
            },
            beforeSend: function () {
                $("#so-luong-qua-tang-update").val(1)
            },
            success: function (data) {
                var $thongTinQuaTang =
                    `
               <div class="bill-box">
                  <div class="img-wrapper">
                      <img src="https://app.vietlixi.com/api/images/` + data.quaTang.anh_dai_dien + `" alt="` + data.quaTang.name + `" class="image-block imaged w100">
                  </div>
                  <div class="price">` + (Number(data.quaTang.don_gia).toLocaleString('vi')) + `</div>
                  <h3>` + data.quaTang.name + `</h3>
              </div>`;
                $("#thong-tin-qua-tang").html($thongTinQuaTang);
                $("#so-luong-qua-tang-update").val(data.quaTang.so_luong);

                $("#actionSheet").modal('hide');
                $("#actionUpdateQuaTang").modal('show');
            },
            error: function (r1, r2) {
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show')
            },
        });
    });

    $(document).on('click', '#action-xem-chi-tiet-nhan-qua', function (e) {
        e.preventDefault();
        $.ajax({
            url: "https://app.vietlixi.com/api/service/load-qua-tang-for-update",
            dataType: "json",
            type: "post",
            data: {
                user,
                quaTang: $("#actionSheet-hidden-value").val()
            },
            beforeSend: function () {
                $("#so-luong-qua-tang-update").val(1)
            },
            success: function (data) {
                var $thongTinQuaTang =
                    `
              <div class="bill-box">
                  <div class="img-wrapper">
                      <img src="https://app.vietlixi.com/api/images/` + data.quaTang.anh_dai_dien + `" alt="` + data.quaTang.name + `" class="image-block imaged w100">
                  </div>
                  <div class="price">` + (Number(data.quaTang.don_gia).toLocaleString('vi')) + `</div>
                  <h3>` + data.quaTang.name + `</h3>
                 
              </div>`;

                var date = new Date(data.quaTang.created);
                var $thongTinChiTiet =
                    `
              <div>
                    <div class="row flex-wrap">
                        <div class="col-3"><h4>Số lượng: </h4></div>
                        <div class="col-9"><span>` + data.quaTang.so_luong + `</span></div>
                    </div>
                    <div class="row flex-wrap">
                        <div class="col-3"><h4>Ngày tạo: </h4></div>
                        <div class="col-9"><span>${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}</span></div>
                    </div>
                    <div class="row flex-wrap">
                        <div class="col-3"><h4>Đơn giá: </h4></div>
                        <div class="col-9"><span>` + (Number(data.quaTang.don_gia).toLocaleString('vi')) + `</span></div>
                    </div>
                    <div class="row flex-wrap">
                        <div class="col-3"><h4>Tổng tiền: </h4></div>
                        <div class="col-9"><span>` + (Number(data.quaTang.tong_tien).toLocaleString('vi')) + `</span></div>
                    </div>
              </div>`;
                $("#thong-tin-qua-tang-chi-tiet").html($thongTinQuaTang);
                $("#thong-so").html($thongTinChiTiet);

                $("#actionSheet").modal('hide');
                $("#actionXemChiTietQuaTang").modal('show');
            },
            error: function (r1, r2) {
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show')
            },
        });
    });


    $(document).on('click', '#action-huy-nhan-qua', function (e) {
        e.preventDefault();

        $.ajax({
            url: "https://app.vietlixi.com/api/service/huy-nhan-qua",
            dataType: "json",
            type: "post",
            data: {
                user,
                quaTang: $("#actionSheet-hidden-value").val()
            },
            beforeSend: function () {
                $("#so-luong-qua-tang-update").val(1)
            },
            success: function (data) {
                $("#modal-icon")
                    .removeClass('text-danger')
                    .addClass('text-success')
                    .html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                $("#noi-dung-thong-bao").html(data.message);
                $("#DialogIconedDanger").modal('show');

                setTimeout(function () {
                    location.reload();
                }, 2000);
            },
            error: function (r1, r2) {
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show')
            },
        });
    })

    $(document).on('click', '#btn-close-update-nhan-qua', function (e) {
        $("#actionUpdateQuaTang").modal('hide');
    })

    $(document).on('click', '#luu-thong-tin-update-qua-tang', function (e) {
        if (Number($("#so-luong-qua-tang-update").val()) <= 0) {
            $("#noi-dung-thong-bao").html('Số lượng tối thiểu là 1');
            $("#DialogIconedDanger").modal('show')
        } else {
            $.ajax({
                url: "https://app.vietlixi.com/api/service/update-qua-tang",
                dataType: "json",
                type: "post",
                data: {
                    user,
                    soLuong: $("#so-luong-qua-tang-update").val(),
                    quaTang: $("#actionSheet-hidden-value").val()
                },
                beforeSend: function () {
                    $("#so-luong-qua-tang-update").val(1)
                },
                success: function (data) {
                    if (data.status === 'success') {
                        $("#DialogIconedDanger .modal-icon").removeClass('text-danger').addClass('text-success');
                        $("#DialogIconedDanger .modal-icon").html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                        $('#actionUpdateQuaTang').modal('hide');

                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        $("#DialogIconedDanger .modal-icon").removeClass('text-success').addClass('text-danger');
                        $("#DialogIconedDanger .modal-icon").html('<ion-icon name="close-circle"></ion-icon>');

                        $('#actionUpdateQuaTang').modal('hide');
                    }

                    $('#DialogIconedDanger #noi-dung-thong-bao').html(data.content);
                    $('#DialogIconedDanger').modal('show');
                },
                error: function (r1, r2) {
                    $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                    $("#DialogIconedDanger").modal('show')
                },
            });
        }
    })
}

if ($(".profile-page").length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/get-profile",
        dataType: "json",
        type: "post",
        data: user,
        beforeSend: function () {
            $(".list-view-profile li strong").html('');
        },
        success: function (data) {
            if (data.user.anhdaidien !== null)
                $("#image-profile").html('<img src="https://app.vietlixi.com/api/images/' + data.user.anhdaidien + '" />')
            $("#fullName").text(data.user.hoten);
            $("#phoneNumber").text(data.user.dien_thoai);
            $("#email").text(data.user.email);
            if (data.user.link !== null) {
                $("#ma-gioi-thieu").html('<a href="' + data.user.link + '" target="_blank">' + data.user.ma_gioi_thieu + '</a>');
                $("#link").text(data.user.link);
            } else $("#ma-gioi-thieu").html('Đang cập nhật');

            $(".pageTitle").html('Thông tin ' + data.user.hoten);
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });

    $(document).on('click', '#sua-ma-gioi-thieu', function (e) {
        e.preventDefault()
        $.ajax({
            url: "https://app.vietlixi.com/api/service/get-profile",
            dataType: "json",
            type: "post",
            data: user,
            beforeSend: function () {
            },
            success: function (data) {
                $("#block-ma-gioi-thieu-cu").html(`
                    <div class="form-control" style="border: none; padding-left: 20px;">
                        <label><strong>Mã giới thiệu cũ: </strong></label>
                        <span>` + data.user.ma_gioi_thieu + `</span>
                    </div>
                `);

                $('#DialogFormMaGioiThieu').modal('show')
            },
            error: function (r1, r2) {
                noiDungThongBao.innerText = r1.responseJSON.message;
                DialogIconedDanger.show();
            },
        })
    })

    $(document).on('click', '.btn-thuc-hien-doi-ma', function (e) {
        e.preventDefault();
        if ($("#ma-gioi-thieu-moi").val() === "") {
            $("#error-quantity").removeClass('hidden').html('Nhập mã giới thiệu mới.')
            $("#ma-gioi-thieu-moi").focus();
        } else {
            $("#error-quantity").addClass('hidden');
            $.ajax({
                url: "https://app.vietlixi.com/api/service/doi-ma-gioi-thieu",
                data: {
                    maGioiThieu: $("#ma-gioi-thieu-moi").val(),
                    user
                },
                dataType: "json",
                type: "post",
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.status === 'success') {
                        $("#DialogIconedDanger .modal-icon").removeClass('text-danger').addClass('text-success');
                        $("#DialogIconedDanger .modal-icon").html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                        $("#DialogFormMaGioiThieu").modal('hide');
                    } else {
                        $("#DialogIconedDanger .modal-icon").removeClass('text-success').addClass('text-danger');
                        $("#DialogIconedDanger .modal-icon").html('<ion-icon name="close-circle"></ion-icon>');
                    }

                    $('#DialogIconedDanger #noi-dung-thong-bao').html(data.content);
                    $('#DialogIconedDanger').modal('show');

                    if (data.status === 'success') {
                        setTimeout(function () {
                            window.location.reload()
                        }, 2000);
                    }
                },
                error: function (r1, r2) {
                    noiDungThongBao.innerText = r1.responseJSON.message;
                    DialogIconedDanger.show();
                },
            });
        }
    })
}

if ($("#edit-profile").length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/get-profile",
        dataType: "json",
        type: "post",
        data: user,
        beforeSend: function () {
        },
        success: function (data) {
            $("#full-name").val(data.user.hoten);
            $("#email-profile").val(data.user.email);
            $("#phone-number").val(data.user.dien_thoai);
            $("#hidden-uid").val(data.user.id);
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });
}

$(document).on('click', '#bank-name', function (e) {
    e.preventDefault()

    $.ajax(settings).done(function (response) {
        $.each(response.data, function (k, value) {
            $('#bank-name').append(`<option value="` + value.shortName + `">` + value.shortName + `</option>`)
        })
    });
})

$(document).on('click', '#btn-save-profile', function (e) {
    e.preventDefault();
    var $loi = validateFormUpdatProfile();
    if ($loi === 0) {
        var $data = new FormData($('#form-profile')[0]);
        $.ajax({
            url: "https://app.vietlixi.com/api/service/save-profile",
            dataType: "json",
            type: "post",
            data: $data,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (data) {
                $("#modal-icon")
                    .removeClass('text-danger')
                    .addClass('text-success')
                    .html('<ion-icon name="checkmark-circle-outline"></ion-icon>');
                $("#noi-dung-thong-bao").html(data.content);
                $("#DialogIconedDanger").modal('show');

                setTimeout(function () {
                    window.location = 'https://vietlixi.com/app-profile.html';
                }, 5000);
            },
            error: function (r1, r2) {
                $("#modal-icon")
                    .removeClass('text-success')
                    .addClass('text-danger')
                    .html('<ion-icon name="close-circle"></ion-icon>');
                $("#noi-dung-thong-bao").html(r1.responseJSON.message);
                $("#DialogIconedDanger").modal('show');
            },
        });
    }
})
$("#so-tien").on('keyup', function () {
    var n = parseInt($(this).val().replace(/\D/g, ''));
    $(this).val(n.toLocaleString());
});

if ($('#body-thong-ke-nhan-tien').length > 0) {
    $.ajax({
        url: "https://app.vietlixi.com/api/service/get-thong-ke-nhan-tien",
        dataType: "json",
        type: "post",
        data: {
            user,
        },
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data.chienDich)
            if (!data.chienDich.length) {
                $("#thong-ke-nhan-tien").append(
                    `<p style="padding-left: 16px">Bạn không có giao dịch nào</p>`
                )
            } else {
                $.each(data.chienDich, function (k, obj) {
                    const classText = obj.trang_thai_postback == 'Khởi tạo' ? 'text-warning' : (obj.trang_thai_postback == 'Hoạt động' ? 'text-success' : 'text-danger');

                    $("#thong-ke-nhan-tien").append(
                        `<a href="#" class="item xem-chi-tiet-postback block-display" data-value="` + obj.id + `" data-type="postback">
            <div class="row">
                <div class="col-3">
                  <div class="text-center"><img src=` + obj.image + ` alt=` + obj.title + ` class="image-block imaged w48"></div>
                </div>
                <div class="col-9">
                    <strong>` + obj.title + `</strong>
                  <div class="row">
                      <div class="col-4"><p>` + obj.trang_thai + `</p></div>
                      <div class="col-8">
                        <div class="price text-success text-right"> +<span class="` + (Number(obj.so_tien_nhan_duoc) == 0 ? 'text-danger' : 'text-success') + `">`
                        + Number(obj.so_tien_nhan_duoc).toLocaleString('vi') + `đ</span>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
          </a>
              `
                    );
                });
            }

            if (data.count < 6) {
                $("#load-more-app-transaction-history-div").addClass("hidden");
            }
        },
        error: function (r1, r2) {
            noiDungThongBao.innerText = r1.responseJSON.message;
            DialogIconedDanger.show();
        },
    });

}


function postCrossDomainMessage(msg) {
    var win = document.getElementById('ifr').contentWindow;
    win.postMessage(msg, "https://vietlixi.com/app-blog-post.html");
}

var postMsg = {"login": "user"}; // this is just example
postCrossDomainMessage(postMsg);

$(document).on('click', '#share-button', function (e) {
    e.preventDefault()

    window.location.href = 'https://www.facebook.com/sharer/sharer.php?u=https://vietlixi.com/'

})

function copy(selector) {
    var $temp = $("<div>");
    $("body").append($temp);
    $temp.attr("contenteditable", true)
        .html($(selector).html()).select()
        .on("focus", function () {
            document.execCommand('selectAll', false, null);
        })
        .focus();
    document.execCommand("copy");
    $temp.remove();

    // Use notify.js to display a notification
    $.notify("Copied!", "success");
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover"
    />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta
            name="apple-mobile-web-app-status-bar-style"
            content="black-translucent"
    />
    <meta name="theme-color" content="#000000" />
    <title>Đăng nhập - Việt Lì xì</title>
    <meta name="description" content="Việt Lì xì" />
    <link
            rel="icon"
            type="image/png"
            href="assets/img/favicon.png"
            sizes="32x32"
    />
    <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="assets/img/icon/192x192.png"
    />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="manifest" href="__manifest.json" />
</head>

<body>
    <?php if(isset($_GET['username']) && isset($_GET['password_hash']) && isset($_GET['auth_key']) ): ?>
    <div class="hidden">
        <form id="form-auto-login">
            <input type="hidden">
            <input
                    type="hidden"
                    value="<?=$_GET['username']?>"
                    name="dienthoai"
            />
            <input
                    name="matkhau"
                    type="hidden"
                    value="<?=$_GET['password_hash']?>"
            />
            <input
                    name="auth_key"
                    type="hidden"
                    value="<?=$_GET['auth_key']?>"
            />
        </form>
    </div>
<?php endif; ?>

    <!-- DialogIconedDanger -->
    <div
            class="modal fade dialogbox"
            id="DialogIconedDanger"
            data-bs-backdrop="static"
            tabindex="-1"
            role="dialog"
    >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-icon text-danger" id="modal-icon">
                    <ion-icon name="close-circle"></ion-icon>
                </div>
                <div class="modal-header">
                    <h5 class="modal-title">Thông báo</h5>
                </div>
                <div class="modal-body" id="noi-dung-thong-bao"></div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn" data-bs-dismiss="modal">Đóng lại</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- * DialogIconedDanger -->
    <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"
    ></script>
    <!-- ========= JS Files =========  -->
    <!-- Bootstrap -->
    <script src="assets/js/lib/bootstrap.bundle.min.js"></script>
    <!-- Ionicons -->
    <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
    ></script>
    <!-- Splide -->
    <script src="assets/js/plugins/splide/splide.min.js"></script>
    <!-- Base Js File -->
    <script src="assets/js/base.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>